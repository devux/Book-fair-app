class AddQuantityToBookFair < ActiveRecord::Migration
  def change
    add_column :book_fairs, :quantity, :string
  end
end
