class CreateBookDetails < ActiveRecord::Migration
  def change
    create_table :book_details do |t|
      t.integer :seriol_no
      t.string :book_name
      t.string :author_name
      t.integer :rate
      t.integer :isbn_no

      t.timestamps null: false
    end
  end
end
