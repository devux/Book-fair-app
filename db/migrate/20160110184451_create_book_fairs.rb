class CreateBookFairs < ActiveRecord::Migration
  def change
    create_table :book_fairs do |t|
      t.integer :seriol_no
      t.string :book_name
      t.string :author_name
      t.string :rate
      t.string :isbn_no

      t.timestamps null: false
    end
  end
end
