class AddEditionToBookFair < ActiveRecord::Migration
  def change
    add_column :book_fairs, :edition, :string
  end
end
