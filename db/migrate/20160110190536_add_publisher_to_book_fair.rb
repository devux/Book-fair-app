class AddPublisherToBookFair < ActiveRecord::Migration
  def change
    add_column :book_fairs, :publisher, :string
  end
end
